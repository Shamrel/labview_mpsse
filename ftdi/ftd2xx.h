	typedef unsigned long	ULONG,  *PULONG;
    typedef unsigned long DWORD;
    typedef ULONG	FT_STATUS;	
    typedef DWORD *PDWORD,*LPDWORD;
    typedef ULONG	FT_HANDLE;
    #define WINAPI __stdcall

    
    typedef struct _ft_device_list_info_node {
    ULONG Flags;
    ULONG Type;
    ULONG ID;
    DWORD LocId;
    char SerialNumber[16];
    char Description[64];
    FT_HANDLE ftHandle;
} FT_DEVICE_LIST_INFO_NODE;
    
    
        FT_STATUS WINAPI FT_GetDeviceInfoList(
		FT_DEVICE_LIST_INFO_NODE *pDest,
		LPDWORD lpdwNumDevs
		);