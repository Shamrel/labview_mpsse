# Utility for configuring Alters's FPGA via USB
 
**version**: 0.1 
**LabVIEW**: 2018  
**OS**: MS Windows  
**author**: Shauerman Alexander <shamrel@yandex.ru>  www.labfor.ru  
**date**: 25.10.2018  
**Device**: [LESO7](http://www.labfor.ru/devices/leso7)  

### details:   
This utility can be used to configure Alters's FPGA via USB.
 It utilises the functionality of the proprietary Multi-Protocol Synchronous
 Serial Engine (MPSSE) architecture to adapt to the Altera Passive Serial (PS) interface.

Especially [for www.habr.com]  
